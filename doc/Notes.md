---
title: Antimicrobial Resistance in Laos 
author: Facundo Muñoz
date: "`r format(Sys.Date(), '%b, %Y')`"
output:
  pdf_document: default
  html_document: default
documentclass: cirad
---



## Context

2018-10-15, Mariline Poupaud

Multiple surveys with different questions depending on the target
population (commercial chicken, backyard chicken, commercial pigs,
backyard pigs, integrated and aquaculture farmers and drugs suppliers).

- Drugs suppliers excluded from analysis (very different questions).

- Certain questions are open (we exclude those).

- There are some questions with multiple possible categories. Treat
each category as a separate question.

- Mariline is going to gather some of the populations together for analysis.

- _Backyard_ questionaries (1b, 2b) and drugs-feed suppliers are
excluded due to strong differences.

- The questionary for __integrated__ pig and fish farmers is actually
double-long, with questions repeated for livestock and for fish. Are
there common questions? Should we account for this additional
structure, rather than keeping these responses as if they were
independent sources?

Structure of questions (all pops):
- Individuals
- Farms
- Knowledge
- Attitudes concerning antibiotic (AB) usage
- Practices on AB usage
- Waste management practices (exclude)
- Quantification of AB usage (exclude)


## Data

- Excel data sheets with categorical and open responses to questions


## Proposed Analyses


- Multiple Correspondence Analysis (MCA)


## Remarks


- No particular deadline
