Antimicrobial Resistance in Laos
================
Facundo Muñoz
October 15, 2018

Multiple Factor Analysis (MFA) of Knowledge-Attitude-Practice survey responses
from several groups of farmers (commercial chicken, backyard chicken,
commercial pigs, backyard pigs, integrated and aquaculture).

Socio-economic variables and farm characteristics included as supplementary.

Derive a typology of farmers based on KAP. Interpret groups and find a 
predictive decision tree for the groups.

- [Clean-up report](reports/cleanup.pdf)

- [Results](reports/MFA_3g.pdf)
