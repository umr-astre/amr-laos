## Interactive
# library(drake)
# source("src/setup.R") # Define your custom code as a bunch of functions.
# source("src/functions.R")
# loadd()

Sys.setenv("DATAVERSE_SERVER" = "dataverse.cirad.fr")

# The workflow plan data frame outlines what you are going to do.
plan <- drake_plan(

  # Load data ---------------------------------------------------------------
  ## Data as read from the file
  ## 476 x 96
  ## The first two variables provide id of the individual (line)
  raw_data = readxl::read_excel(
    file_in(!!here::here("data/Merged questionnaires.xlsx")),
    na = c("", "NA", "na", "Na")
  ),


  # Parameters --------------------------------------------------------------

  ## List of re-codification rules for nominal variables
  nominal_recodes = list_nm_rx(),


  # Derived datasets --------------------------------------------------------

  ## Remove some columns and clean thing up
  ## 476 x 92 (including 2 scores)
  clean_data = cleanup(raw_data),

  dataverse_data = {
    ## Until the data is published, this need to be run locally with
    ## a properly set DATAVERSE_KEY in .Renviron
    if (Sys.getenv("DATAVERSE_KEY") != "") {
      tmp <- tempfile(fileext = ".dta")
      get_file("kap_amr_laos.tab", dataset = "doi:10.18167/DVN1/UM7YUS") %>%
        writeBin(tmp)
      read.csv(tmp)
    }
  },

  ## Extract the scores for knowledge and practices
  ## These are actually two computed variables from the original responses
  ## 476 x 2
  scores_data = clean_data %>%
    select(matches("score"))  %>%
    structure(multiple_response_levels = NULL) %>%
    set_names(gsub("x..._", "", names(.))),


  ## Expand and recode variables
  ## 476 x 171
  processed_data = expand_recode(clean_data, nominal_recodes),

  ## Select variables for each topic and perform some final preparation
  ## (logical as factor, select only categorical vars, abbr. varnames)
  ## 476 x 165
  kap_data = processed_data[, topic_vars$variable] %>%
    abbreviate_varnames() %>%
    ## convert logical to factor (necessary for MCA)
    mutate_if(is.logical, as.factor) %>%
    as_tibble(),

  ## kap_data + scores_data
  kap_scores = bind_cols(
    kap_data,
    scores_data %>%
      select(`score_knowledge`, `score_practices`) %>%
      abbreviate_varnames()
  ),

  ## Ventilation
  ## To be consistent with MCAs, we need to ventilate categories with
  ## a proportion of observations under 5%. We had also imputed missing
  ## values by average, but in this case we might want to keep them as
  ## a group, as there are meaningful missing values.
  ## Supplementary variables are not ventilated NAs groups are not ventilated
  ## either
  kap_ventilated = ventil.tab(
    ## Needed, as is asks for dat[, i] counting on default drop
    as.data.frame(kap_scores),
    level.ventil = 0.05,
    row.w = NULL,
    ind.sup = NULL,
    quali.sup = which(topic_vars$topic %in% c("socio_demo", "farm")),
    quanti.sup = ncol(kap_scores) + -1:0  # last 2 are the scores
  ),
  # all.equal(as.data.frame(kap_scores), kap_ventilated)

  ## Data.frame of selected variable names in processed_data, their
  ## abbreviation and their corresponding topic. 8 variables are
  ## dropped because either irrelevant or numeric:
  ## setdiff(names(processed_data), topic_vars$variable)
  ## "id, village_translation, x9_age_years, type_of_animals_kept,
  ## x19_ii_1_other_animal_kept, x24_ii_3_number_of_animals,
  ## x86_iii_5_another_way_to_treat_your_sick_chicken,
  ## x165_v_12_gt_1_yes_if_yes_ho" 165 x 3
  topic_vars = topic_variables_df(
    names(processed_data %>% select_if(~!is.numeric(.))),
    socio_demo = quos(target_pop, x5_province:type_of_animals_kept, type_of_animals_simple),
    farm = quos(x28_ii_5_species:x70_ii_17_do_you_record_farm_information),
    knowledge = quos(x74_iii_1_are_you_familiar_with_antibiotics:x85_iii_4_5_withdrawal_time_is_necessary, x90_iii_6_can_we_find_ab_in_feed:x97_iii_8_5_chicken_meat_or_in_the_eggs_and_you_can_consume_ab),
    attitude = quos(x99_iv_1_1_protect_from_any_diseases:x121_iv_5_learning_more_about_antibiotics),
    practice = quos(x123_v_1_animal_is_sick_what_do_you_first_do:x168_v_15_where_do_you_buy_medicated_feed)
  ),

  topics = topic_vars %>%
    mutate(topic = fct_inorder(topic)) %>%  ## Keep the ordering as in the dataset
    count(topic, name = "Nvar"), #%>%
  # bind_rows(
  #   tibble(
  #     topic = c("sc_knwldge", "sc_prctcs"),
  #     Nvar = rep(1L, 2)
  #   )
  # )



  # Results -----------------------------------------------------------------

  ## Multiple Factor Analysis
  resmfa = kap_ventilated %>%
    imputeMFA(
      group = c(topics$Nvar, 1, 1),
      ncp = 12,
      type = c(rep("n", nrow(topics)), "c", "c"),  # n for categorical variables, c for quantitative
      num.group.sup = c(1, 2, 6, 7),  # socio_demo, farm and scores as supplementary
    ) %>%
    with(
      .,
      MFA(
        completeObs,
        group = c(topics$Nvar, 1, 1),
        type = c(rep("n", nrow(topics)), "c", "c"),  # n for categorical variables, c for quantitative
        name.group = c(as.character(topics$topic), "sc_knwldge", "sc_prctcs"),
        num.group.sup = c(1, 2, 6, 7),  # socio_demo, farm and scores as supplementary
        ncp = 12,
        graph = FALSE
      )
    ),

  hcpc = target(
    HCPC(
      resmfa,
      nb.clust = n_groups,  # Cut tree at suggested level
      min = 3,        # Least possible number of clusters suggested
      graph = FALSE,  # Do not plot at this moment
      proba = 0.01    # Significance threshold to characterise the categories
    ),
    transform = map(n_groups = !!(3:5))
  ),



  # Reports -----------------------------------------------------------------

  ## Dataset to be made available in the Dataverse
  ## TODO: publish the attribute multiple-response-levels as well.
  publish_data = write.csv(
    clean_data,
    row.names = FALSE,
    file_out(!!here::here("reports/kap_amr_laos.csv"))
  ),

  cleanup_report = rmarkdown::render(
    knitr_in("src/cleanup.Rmd"),
    output_file = file_out(!!here("reports/cleanup.pdf")),
    quiet = TRUE
  ),
  mca = rmarkdown::render(
    knitr_in("src/MCA.Rmd"),
    output_file = file_out(!!here("reports/MCA.pdf")),
    quiet = TRUE
  ),

  mfa = rmarkdown::render(
    knitr_in("src/MFA.Rmd"),
    output_file = file_out(!!here("reports/MFA.pdf")),
    params = list(
      n_mod_partial = 20
    ),
    quiet = TRUE
  ),

  hcpc_mfa = target(
    rmarkdown::render(
      knitr_in("src/HCPC_MFA.Rmd"),
      output_file = file_out(!!here("reports", paste0(quote(hcpc), ".pdf"))),
      params = list(
        hcpcmfa = hcpc,
        n_mod_disp = 50
      ),
      quiet = TRUE
    ),
    transform = map(hcpc)
  ),
)
