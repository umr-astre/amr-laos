## Functions related with ventilation of under-represented modalities
## Extracted from within FactoMineR::MCA()

ventil.tab <- function(tab, level.ventil = 0.05, row.w = NULL,
                       ind.sup = NULL, quali.sup = NULL, quanti.sup = NULL) {
  if (is.null(row.w))
    row.w <- rep(1, nrow(tab) - length(ind.sup))
  col.var <- 1:ncol(tab)
  if (!is.null(c(quali.sup, quanti.sup)))
    col.var = col.var[-c(quali.sup, quanti.sup)]
  for (i in col.var) {
    if (is.factor(tab[, i])) {
      tab[, i] <- ventilation(tab[, i], level.ventil = level.ventil,
                              row.w = row.w, ind.sup = ind.sup)
    }
    if (is.ordered(tab[, i])) {
      tab[, i] <- ventilation.ordonnee(tab[, i], level.ventil = level.ventil,
                                       row.w = row.w, ind.sup = ind.sup)
    }
  }
  return(tab)
}
ventilation <- function(Xqual, level.ventil = 0.05, row.w = NULL,
                        ind.sup = NULL) {
  if (!is.factor(Xqual))
    stop("Xqual should be a factor \n")
  modalites <- levels(Xqual)
  if (length(modalites) <= 1)
    stop("not enough levels \n")
  if (is.null(ind.sup)) {
    ind.act <- (1:length(Xqual))
  }
  else {
    ind.act <- (1:length(Xqual))[-ind.sup]
  }
  tabl <- table(Xqual[ind.act])
  if (!is.null(row.w)) {
    for (j in 1:nlevels(Xqual)) tabl[j] <- sum((Xqual[ind.act] ==
                                                  levels(Xqual)[j]) * row.w, na.rm = TRUE)
  }
  selecti <- (tabl/sum(tabl, na.rm = TRUE)) < level.ventil
  if (sum(selecti) == length(modalites))
    return(Xqual)
  if (!any(selecti))
    return(Xqual)
  else {
    lesquels <- modalites[!selecti]
    if (length(lesquels) == 1)
      return(Xqual)
    else {
      prov <- factor(Xqual[(Xqual %in% lesquels)],
                     levels = lesquels)
      prov <- table(prov)
      proba <- prov/sum(prov)
      for (j in modalites[selecti]) {
        Xqual[which(Xqual == j)] <- sample(lesquels,
                                           sum(Xqual == j, na.rm = TRUE), replace = TRUE,
                                           prob = proba)
      }
      Xqualvent <- factor(as.character(Xqual))
    }
  }
  return(Xqualvent)
}
ventilation.ordonnee <- function(Xqual, level.ventil = 0.05,
                                 ind.sup = NULL, row.w = NULL) {
  if (!is.ordered(Xqual))
    stop("Xqual must be ordered \n")
  mod <- levels(Xqual)
  if (length(mod) <= 1)
    stop("not enough levels \n")
  if (is.null(ind.sup)) {
    ind.act <- (1:length(Xqual))
  }
  else {
    ind.act <- (1:length(Xqual))[-ind.sup]
  }
  tabl <- table(Xqual[ind.act])
  if (!is.null(row.w)) {
    for (j in 1:nlevels(Xqual)) tabl[j] <- sum((Xqual[ind.act] ==
                                                  levels(Xqual)[j]) * row.w, na.rm = TRUE)
  }
  selecti <- (tabl/sum(tabl)) < level.ventil
  if (!any(selecti))
    return(Xqual)
  else {
    numero <- which(selecti)
    while (any((tabl/sum(tabl)) < level.ventil)) {
      j <- which(((tabl/sum(tabl)) < level.ventil))[1]
      K <- length(mod)
      if (j < K) {
        if ((j > 1) & (j < K - 1))
          levels(Xqual) <- c(mod[1:(j - 1)], paste(mod[j],
                                                   mod[j + 1], sep = "."), paste(mod[j],
                                                                                 mod[j + 1], sep = "."), mod[j + 2:K])
        if (j == 1)
          levels(Xqual) <- c(paste(mod[j], mod[j +
                                                 1], sep = "."), paste(mod[j], mod[j +
                                                                                     1], sep = "."), mod[j + 2:K])
        if (j == (K - 1))
          levels(Xqual) <- c(mod[1:(j - 1)], paste(mod[j],
                                                   mod[j + 1], sep = "."), paste(mod[j],
                                                                                 mod[j + 1], sep = "."))
      }
      else {
        levels(Xqual) <- c(mod[1:(j - 2)], paste(mod[j -
                                                       1], mod[j], sep = "."), paste(mod[j - 1],
                                                                                     mod[j], sep = "."))
      }
    }
  }
  return(Xqual)
}
