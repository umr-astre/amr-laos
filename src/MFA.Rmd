---
title: "Multiple Factor Analysis of joint survey responses"
author: "Facundo Muñoz"
date: "`r format(Sys.Date(), '%e %B, %Y')`"
# knit: (function(inputFile, encoding, knit_root_dir) {
#     rmarkdown::render(
#       inputFile,
#       encoding = encoding,
#       knit_root_dir = knit_root_dir,
#       output_dir = "../report",
#       output_format = "pdf_document"
#     )
#   })
output:
  pdf_document:
    template: null
    fig_crop: false
documentclass: cirad
bibliography: amr_laos.bib
params:
  n_mod_partial: 20
---


```{r load-pkgs_fun, eval = interactive(), cache = FALSE}
source("src/setup.R")
source("src/functions.R")
message("Assuming interactive session")
```

```{r setup, include=FALSE, cache = FALSE}
knitr::opts_chunk$set(
  echo = FALSE,
  cache = FALSE
)

theme_set(theme_bw())
```

```{r load, cache = FALSE}
loadd(
  kap_data,
  topic_vars,
  resmfa
)

```

## Introduction 

This method integrates the previous MCA analyses by sets of related
variables into a single analysis that takes into account the grouped
structure of the variables.

In summary, we consider the full table with `r nrow(kap_data)`
individuals and `r nrow(topic_vars)` categorical variables with a total
of `r sum(vapply(kap_data, nlevels, 1))` levels.
Furthermore, we include the  calculated scores for knowledge and
practices as two separate topics with one quantitative variable each.
<!-- , organised -->
<!-- in the groups shown in Table \ref{topic-Kj} -->

We are going to compute the principal factors using all variable
groups except for the __socio-economic__, __farm__ and both __scores__
variables which are included as __supplementary__.

First, we consider the structure of missing values in the dataset,
as this has a strong influence in the results (yes, I have already
tried to ignore missing values).

In particular, there are `r kap_data %>%  select(topic_vars %>% filter(topic == "attitude") %>% pull(abbr_variable)) %>% pmap_lgl(., ~all(is.na(c(...)))) %>% sum`
individuals for which all _attitude_ variables are missing and were
removed for the previous partial analysis that need to be included
in the present analysis.

However, these individuals also have several missing values in
_practice_ and _knowledge_ variables, which creates a strong
association between these groups, since missing values are treated as
a category. Furthermore, this category for several variables end up
driving the first principal component.

Yet, these missing values correspond to individuals for whom the
questions are not really applicable. Thus, it makes sense to 
distinguish them as a group.

__The fact that the first principal component is related with this
group means that the strongest classification criteria is given
by this group's membership.__

Note that in the previous MCA analyses, missing values were replaced
by the relative proportion of each modality. Moreover, in the case of
the attitude variables, 14 individuals with missing values in all
variables were removed. Thus, some differences in the partial analyses
are to be expected.

<!-- The appropriate course of action depends on the situation. -->
<!-- We can either: -->

<!-- - Remove these individuals entirely if their responses are generally  -->
<!-- not reliable. -->

<!-- - Use imputation methods to fill in the missing values. However, this -->
<!-- is only appropriate if these NAs are actual missing values rather than -->
<!-- "Not Applicable" -->

<!-- - If missing values represent "Not Applicable" responses, it might  -->
<!-- make sense to leave them as a supplementary category as they are. -->


```{r visualise-missing-topic, warning = FALSE, message = FALSE, fig.cap = "Representation of the dataset by topic (not including scores), with Missing Values highlighted as blanks.", fig.width = 10, fig.height = 7}
p <- vis_miss(kap_data, cluster = TRUE)

p$data <- 
  p$data %>% 
  left_join(
    topic_vars %>% select(topic, variable = abbr_variable),
    by = "variable"
  ) %>% 
  mutate(
    topic = fct_relevel(
      fct_inorder(replace(topic, which(p$data$value), "MISSING")),
      "MISSING",
      after = Inf
    ) %>% fct_rev()
  )


## https://cran.r-project.org/web/packages/gginnards/vignettes/user-guide-2.html
suppressMessages(
  gginnards::delete_layers(p, "GeomRaster") +
    geom_raster(aes(fill = topic)) +
    scale_fill_manual(
      "Topic",
      values = c("white", hcl.colors(5))
      # labels = letters[1:4]
    ) +
    theme(
      axis.text.x = element_text(size = 5, angle = 90)
    )
)

```

## Results

Figure \ref{fig:eigen-mfa} shows that the variability of this dataset
spreads into several dimensions. We require 
`r which.max(resmfa$eig[,3] > 50) - 1` dimensions (out of a total of 
`r nrow(resmfa$eig)` to describe only half of the total variability!!.

Yet, there is a dominant first axis of variation, and to a lesser
extent a second one.
The subsequent axes form a cloud of points with slowly decreasing
diameters.


```{r eigen-mfa, fig.cap = "\\label{fig:eigen-mfa}Percent variance explained by Principal Factor and average value (red horizontal line). Dark bars accumulate 50% of the variance."}

# max_dim <- nrow(resmfa$eig)
max_dim <- 50

barplot(
  resmfa$eig[seq.int(max_dim), 2],
  names = seq.int(max_dim),
  cex.names = 0.8,
  col = c("black", "grey")[(resmfa$eig[seq.int(max_dim), 3] > 50) + 1]
)

abline(
  h = 1 / nrow(resmfa$eig) * 100,
  col = 2,
  lty = 2
  )
```



Figure \ref{fig:MFA-plot} shows the projection of individuals
and categories on the first six factorial planes, while Figure \ref{fig:MFA-pairs-plot-individuals} shows the same
projection over all factor planes in the first twelve dimensions.
Note that the first three factorial dimensions clearly discriminate
the four groups.

```{r MFA-plot, fig.width=10, fig.height=15, fig.cap = "\\label{fig:MFA-plot}MFA plot of individuals. First six factor planes."}

factor_planes <- function(x) data.frame(rbind(2*x-1, 2*x))

## Include the cluster variable with the coordinates of individuals
## for plotting
do.call(
  grid.arrange,
  map(
    factor_planes(1:6),
    ~ mfa_project_categories(resmfa, dims = .) %>% 
      ggplot(aes(x, y)) +
      geom_text(
        aes(label = ind),
        alpha = .3,
        data = mca_project_individuals(resmfa, dims = ., key = "ind")
      ) +
      theme_mca(resmfa, dims = .) +
      coord_fixed()
  )
)
```


```{r MFA-pairs-plot-individuals, fig.width=15, fig.height=15, fig.cap = "\\label{fig:MFA-pairs-plot-individuals}MFA plot of individuals. All factor planes in the first twelve dimensions."}

data.frame(resmfa$ind$coord[, 1:12]) %>%
  ggpairs(
    lower = list(combo = wrap(ggally_facethist, bins = 15)),
    upper = list(continuous = wrap(ggally_cor, stars = FALSE, digits = 2))
    )
```

```{r MFA-pairs-plot-categories, fig.width=15, fig.height=15, fig.cap = "\\label{fig:MFA-pairs-plot-categories}MFA plot of categories. All factor planes in the first six dimensions."}

(p <- 
  mfa_project_categories(resmfa, dims = 1:12) %>%
  mutate(variable = as.character(variable)) %>% 
  left_join(
    topic_vars %>% select(variable = abbr_variable, topic),
    by = "variable"
  ) %>% 
  ggpairs(
    columns = grep("^Dim|topic", names(.)),
    mapping = aes(color = topic),
    lower = list(
      continuous = wrap(ggally_points, alpha = .5),
      combo = wrap(ggally_facethist, bins = 15)
    ),
    upper = list(continuous = "blank")
  ))


# # plotly_json(p)
# require(plotly)
# plotly::ggplotly(p) %>% style(text = mfa_project_categories(resmfa, dims = 1:2)$category)
```


The main axis of variation is determined by the _practices_, _knowledge_
and _attitude_ (Fig. \ref{fig:groups-loadings}), which are strongly
correlated topics (i.e. individuals with similar practices also share
similar knowledge and attitudes).

On the other hand, the _farm_ characteristics is the topic most 
associated with the second axis of variation, slightly associated
with _practices_ and with the _socio-demographic_ variables.

The third main axis of variation is moderately associated
with _attitude_ variables, while the fourth with all the rest.
The subsequent dimensions are not clearly driven by any particular
topic.



```{r groups, fig.cap = "\\label{fig:groups-loadings}Contributions of topics to the first six factorial planes", fig.height=9}
# plot.MFA(resmfa, choix = "group")

do.call(
  grid.arrange,
  c(
    map(
      factor_planes(1:6),
      ~bind_rows(
        resmfa$group$coord[, .] %>%
          data.frame() %>% 
          rownames_to_column(var = "group") %>% 
          add_column(type = "active"),
        resmfa$group$coord.sup[, ., drop = FALSE] %>% 
          data.frame() %>% 
          rownames_to_column(var = "group") %>% 
          add_column(type = "suppl.")
      ) %>% 
        ggplot(aes_(as.name(names(.)[2]), as.name(names(.)[3]))) +
        geom_point() +
        geom_text_repel(
          aes(label = group, col = type),
          # vjust = 1,
          hjust = 0,
          nudge_x = .05,
          # nudge_y = -.01,
          xlim = c(-1, 2),
          direction = "y",
          force = .5,
          show.legend = FALSE
        ) +
        scale_colour_grey(start = 0, end = .5) +
        lims(x = 0:1, y = 0:1) +
        coord_fixed(clip = "off") +
        theme_mca(resmfa, dims = .)
    ),
    list(ncol = 2)
  )
)


```



Figure \ref{fig:partial-axes} shows the __relationship between
the principal dimensions of the partial MCA analyses and the 
global principal factors__.

We can see how the main axes of _attitude_, _knowledge_ and _practice_
are strongly correlated between them and with the first principal
dimension. 

The second principal dimension oposes the first factor of _farm_ and 
the second of _practice_ (which are somewhat correlated) with the second
factor of _knowledge_ variables.

The _socio-demographic_ variables are mostly orthogonal to this
plane, up to the fourth dimension, which appears correlated with the
second dimension of the _knowledge_ topic.

Finally, the third dimension is mainly associated with the second
factor of the _attitude_ and with the third factor of the
_practice_ variables which are positively correlated.


```{r partial-axes, fig.width = 10, fig.height = 12.5, fig.cap = "\\label{fig:partial-axes}Correlation between partial dimensions and principal axes."}
# str(resmfa, 1)

# par(mfrow = c(2, 1))
# plot.MFA(resmfa, axes = 1:2, choix = "axes", select = "contrib 20")
# plot.MFA(resmfa, axes = 1:2, choix = "axes")
# plot.MFA(resmfa, axes = 3:4, choix = "axes")

plot_partial_subaxes <- function(x) {
   resmfa$partial.axes$coord[, x] %>% 
  data.frame() %>% 
  rownames_to_column(var = "dim_topic") %>% 
  separate(dim_topic, into = c("subdim", "Topic"), sep = "\\.") %>% 
  filter(subdim %in% paste0("Dim", 1:4)) %>% 
  setNames(
    replace(names(.), grep("Dim", names(.)), c("x", "y"))
  ) %>% 
  ggplot() +
  theme_mca(resmfa, dims = x) +
  geom_segment(
    aes(x = 0, y = 0, xend = x, yend = y, colour = Topic),
    arrow = arrow(length = unit(0.1, "inches"))
  ) +
  ggforce::geom_circle(
    data = data.frame(x0 = 0, y0 = 0, r = 1),
    aes(x0 = x0, y0 = y0, r = r),
    colour = "grey50"
  ) +
  geom_text_repel(
    aes(x, y, colour = Topic, label = subdim),
    segment.colour = "grey80"
  ) +
  expand_limits(x = c(-1, 1), y = c(-1, 1)) +
  coord_fixed() +
  scale_color_hue(
    guide = guide_legend(
      override.aes = list(
        arrow = arrow(length = unit(0, "pt")),
        label = ""
      )
    )
  )
}

# plot_partial_subaxes(c(1,4))

do.call(
  grid.arrange,
  c(map(factor_planes(1:6), plot_partial_subaxes))
)


```


\clearpage

### Projections of the supplementary variables

Assess the relationship between selected supplementary variables and the
principal dimensions and identified clusters.

Notes concerning the variables of interest (see clean up report for details):

1. Some variables with multiple levels such as `13. Occupation` or `28. Species`
have multiple selected choices for some respondents (e.g. Farmer, Private,
Worker, Retired, ...). Thus, we needed to split the variables into as many of
binary variables as the number of possible choices in order to perform the MFA.
As a result, we can only make plots for each of the binary derived variables
here.

1. Variables `type of animals kept` and `53 Disease` have been deemeed
_irrelevant_ and removed from analysis during the clean-up.

1. Variables `22. Is your farm contracted ...` and `34. Where do you sell...`
had not been consistently posted to all groups and have been removed.

1. Variable `24. Number of animals` is numeric and has been removed.

```{r target-supvars}

tgt_vars <- paste0("X", c(5, 6, 8, 9, 12, 13, 17, "TypeOf", 22, 28, 44, 65, 66)) %>% gsub("XType", "Type", .)
rgx_vars <- paste0(paste(tgt_vars, collapse = "|"), ")\\D")
tgt_varidx <- grep(rgx_vars, topic_vars$abbr_variable)

tgt_varnames_socio_demo <- topic_vars %>% 
  slice(tgt_varidx) %>% 
  filter(topic == "socio_demo") %>% 
  pull(abbr_variable) %>% 
  ## Remove a few uninteresting levels of occupation
  grep("X13Occup(Worker|House|Retird|Other)", ., invert = TRUE, value = TRUE)

tgt_varnames_farm <- topic_vars %>% 
  slice(tgt_varidx) %>% 
  filter(topic == "farm") %>% 
  pull(abbr_variable) %>% 
  ## Remove a few uninteresting levels of species
  grep("X28Spec(Carp|Cross|Unknw|Other)", ., invert = TRUE, value = TRUE)


```


```{r projections_supplementaires-socio_demo, fig.width = 10, fig.height = 14, fig.cap = cap}
cap <- "\\textbf{socio\\_demo}: Projections of the barycenters of selected supplementary categories in the main cloud of individuals with 95% CIs."
plotellipses(
  resmfa,
  keepvar = tgt_varnames_socio_demo,
  label = "ind.sup"
) +
  latticeExtra::layer(panel.abline(v = 0, lty = 2)) +
  latticeExtra::layer(panel.abline(h = 0, lty = 2))
```


```{r projections_supplementaires_farm, fig.width = 10, fig.height = 14, fig.cap = cap}
cap <- "\\textbf{farm}: Projections of the barycenters of selected supplementary categories in the main cloud of individuals with 95% CIs."

plotellipses(
  resmfa,
  keepvar = tgt_varnames_farm,
  label = "ind.sup"
) +
  latticeExtra::layer(panel.abline(v = 0, lty = 2)) +
  latticeExtra::layer(panel.abline(h = 0, lty = 2))

```



\clearpage

## Partial results

While the very rare (< 5%) modalities of categorical variables were
"ventilated", as in the MCA analyses, the treatment of missing values
was different. In the previous MCA analyses, missing values were
replaced by the relative proportion of each modality. Moreover, in the
case of the attitude variables, 14 individuals with missing values in
all variables were removed. Here instead we treat missing values as an
additional category for the reasons explained in the beginning. Thus,
some differences in the partial analyses are to be expected.

This behaviour can be defined different for different variables if
needed.


```{r MCA-topic-topn, results='asis'}
# Dynamically knit the following two chunks for each of the topics
# https://cran.rstudio.com/web/packages/knitr/vignettes/knit_expand.html
walk(
  ## Use the original 5 topics (excluding the scores)
  names(resmfa$separate.analyses)[1:5],
  ~ knit_expand(
    text = c(
      '```{r MCA-{{.}}-plot-topn, fig.width=10, fig.height=15, fig.cap = cap}',
      'cap <- gsub("_", "", "\\\\label{fig:MCA-{{.}}-plot-topn}\\\\textbf{ {{.}}} variables: MCA representation of most important modalities on the first two factor planes.")',
      'plot_most_categories_two_factor_planes(resmfa$separate.analyses[["{{.}}"]], n = params$n_mod_partial)',
      '```\n',
      '\n',
      '```{r MCA-{{.}}-plot-topn-by-axis, fig.width=10, fig.height=15, fig.cap = cap}',
      'cap <- gsub("_", "", "\\\\label{fig:MCA-{{.}}-plot-topn-axis}\\\\textbf{ {{.}}} variables: Most contributing modalities for the first four factorial dimensions.")',
      'plot_most_categories_four_dimensions(resmfa$separate.analyses[["{{.}}"]], n = params$n_mod_partial)',
      '```\n'
    )
  ) %>%
    knit_child(text = ., quiet = TRUE) %>%
    cat(sep = '\n')
)


```




\clearpage


## Conclusions




## References