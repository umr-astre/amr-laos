# Load our packages in the usual way.
pacman::p_load(
  broom,
  dataverse,
  DiagrammeR,
  drake,
  here,
  janitor,
  knitr,
  FactoMineR,
  factoextra,  # ggplot2
  GGally,
  gginnards,
  glue,
  ggrepel,
  ggtext,
  gridExtra,
  knitr,
  MASS,
  missMDA,
  skimr,
  tidyverse,
  visdat,
  xtable
)

pkgconfig::set_config("drake::strings_in_dots" = "literals") # New file API
#
# # Your custom code is a bunch of functions.
# create_plot <- function(data) {
#   ggplot(data, aes(x = Petal.Width, fill = Species)) +
#     geom_histogram(binwidth = 0.25) +
#     theme_gray(20)
# }

knitr::opts_chunk$set(
  echo = FALSE,
  cache = TRUE
)

theme_set(theme_bw())
